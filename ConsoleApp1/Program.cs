﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp1
{
    //autor: César P. Franco
    class Program

    {

        static List<CLIENTE> Listaclientes;
        static List<Factura> LaFactura;
        static void Main(string[] args)
        {
            //Segunda parte de la tarea aquí mismo porque yolo jaja

            Listaclientes = new List<CLIENTE>
            {
            new CLIENTE() { idCliente=01,Nombre="Frank"},
            new CLIENTE(){ idCliente=02,Nombre="PACO"},
            new CLIENTE(){ idCliente=03,Nombre="ANDREA"},
            new CLIENTE(){ idCliente=04,Nombre="LAURA"},
            new CLIENTE(){ idCliente=05,Nombre="M.S.MERO"}
            };

            LaFactura = new List<Factura>
            {
                new Factura { idcliente=01,fecha=new DateTime(2019,10,2), observacion="factura realizada 1", total=10000,venta=5,},
                new Factura { idcliente=02,fecha=new DateTime(2020,05,9), observacion="prob", total=20000,venta =4 },
                new Factura { idcliente=03,fecha=new DateTime(2020,06,10), observacion="factura realizada 3", total=30000,venta=1, },
                new Factura { idcliente=04,fecha=new DateTime(2021,10,10), observacion="factura realizada 4", total=40000,venta=7 },
                new Factura { idcliente=05,fecha=new DateTime(2021,01,12), observacion="factura realizada 5", total=50000,venta=2 },
            };

            /*-----------------------------------------------------------------------------------------------------------------------------*/

            var observacion =
                LaFactura.Where(LaFactura => LaFactura.observacion.Contains("Prob", StringComparison.CurrentCultureIgnoreCase));
            foreach(var item in observacion)
                
            { Console.WriteLine("El numero del clienteque tiene una observacion es: "+item.idcliente); }


            /*-----------------------------------------------------------------------------------------------------------------------------*/
            // •	Mostrar en consola las ventas más antiguas.

            var ventaMASantigua =
                (from venta in LaFactura
                 join person in Listaclientes
                 on venta.idcliente equals person.idCliente
                 //agg fecha minima para la consulta 
                 
                 orderby venta.fecha ascending
                    select new { person.Nombre, venta.fecha}).Take(1);


                 foreach (var item in ventaMASantigua)

                    {
                Console.WriteLine(item.Nombre+" Fue nuestra compra mas antigua en la fecha: "+item.fecha);
                      }




    /*-----------------------------------------------------------------------------------------------------------------------------*/
    //•	Mostrar en consola las ventas realizadas hace menos de 1 año.
    var ventaESTEaño =
                (from venta in LaFactura
                 join person in Listaclientes
                 on venta.idcliente equals person.idCliente
                 where venta.fecha >=new DateTime (2020,12,31)
                 orderby venta.fecha descending
                 select new { person.Nombre, venta.fecha});

            foreach (var item in ventaESTEaño)

            {
                Console.WriteLine(item.Nombre+" Ha comprado este año "+item.fecha);
            }

            /*-----------------------------------------------------------------------------------------------------------------------------*/

            //•	Mostrar en consola el cliente y la cantidad de ventas realizadas.
            var ventaMAYORycuanta =
                (from person in Listaclientes
                 join num in LaFactura
                 on person.idCliente equals num.idcliente
                 where person.idCliente == num.idcliente
                 orderby num.venta descending
                 select new { person.idCliente, person.Nombre, num.venta }).Take(1);


            foreach (var item in ventaMAYORycuanta)
            {
                Console.WriteLine("la venta más alta fue de: " + item.Nombre + " con:" + item.venta + "ventas");
            }



            /*-----------------------------------------------------------------------------------------------------------------------------*/


            //mayor venta
            var ventaMAYOR =
                (from person in Listaclientes
                 join num in LaFactura
                 on person.idCliente equals num.idcliente where person.idCliente == num.idcliente
                 orderby num.venta descending
                 select new { person.idCliente, person.Nombre,num.venta }).Take (1) ;


            foreach(var item in ventaMAYOR)
            {
                Console.WriteLine("la venta más alta fue de: "+item.Nombre);
            }




            /*-----------------------------------------------------------------------------------------------------------------------------*/
            var mayorMonto =
            (from mayor in Listaclientes
            join num in LaFactura on mayor.idCliente equals num.idcliente
                    orderby num.total descending
            select new { mayor.Nombre, num.total }).Take(3);
                        
                        Console.WriteLine( "Lista de clientes que mas facturaron: ");
            foreach (var resuelto in mayorMonto)
            {
                    
                Console.WriteLine("Nombre: " + resuelto.Nombre +" Facturó: "+resuelto.total);

            }
            
            //where mayor.idCliente==01 


            /*-----------------------------------------------------------------------------------------------------------------------------*/




            var menorMonto =
            (from mayor in Listaclientes
             join num in LaFactura on mayor.idCliente equals num.idcliente
             orderby num.total ascending
             select new { mayor.Nombre, num.total }).Take(3);

                Console.WriteLine("Lista de clientes con menores montos por pagar: ");
            foreach (var resuelto in menorMonto)
            {

                Console.WriteLine("Nombre: " + resuelto.Nombre + " Facturó: " + resuelto.total);
                
            }











            //--------------------------------------------------------------------------------PRIMERA PARTE DE LA TAREA POO-LINQ ---------------------------------------------------------------
            //--------------------------------------------------------------------------------Estudiante:César P. Franco---------------------------------------------------------------



            // //List<Nprimos> Primos = new List<Nprimos>();

            // //lista de numeros al azar, tiene 20 datos                    |
            // Console.WriteLine("Hello World!");                            |   
            // int[] array = new int[20];                                    |  
            // int Uno = 0;                                                  |    
            // Boolean esprimo = true;                                       |
            // int par;                                                      |obligatorio este es el array que genera los datos al azar
            // int impar;                                                    |
            // int suma=0;                                                   |
            // Random rnd = new Random();                                    |
            // for (int i = 0; i < 20; i++)                                  |

            // {                                                             |
            //     array[i] = rnd.Next(0, 20);                               |
            //     Console.WriteLine(array[i] + " ");                        |       
            //}

            // /*------------------------------------------------------------------------------------------------------------*/
            // //ESTA PARTE DEL CODIGO MUESTA LA SUMATORIA DE LOS NUMEROS UNICOS 
            // //EN parte es casi igual por lo que usaremos las mismas variables mas la de suma 

            // var sumaUnicos =
            //     from dato in array
            //     group dato by dato into lasuma
            //     where lasuma.Count() == 1
            //     select lasuma;
            // //mostrar con el foreach
            // foreach (var item in sumaUnicos)
            // {
            //     suma =suma+item.Key;

            // }
            // Console.WriteLine("La suma de los números que son unicos es: " +suma);

            /*------------------------------------------------------------------------------------------------------------*/
            ////esta parte del cogido muestra los números unicos 
            //var valor =
            //        from dato in array
            //        group dato by dato into UNICO
            //        where UNICO.Count() == 1
            //        select UNICO;

            //Console.WriteLine("Números unicos: ");

            //foreach (var item in valor)
            //{ Console.WriteLine(item.Key+""); }
            /*------------------------------------------------------------------------------------------------------------*/

            ////Mastrar en consola los datos desendentes
            //var orden =
            //    from num in array
            //    orderby num descending
            //    select num;

            //Console.WriteLine("desendente:");
            //foreach (var item in orden)
            //{
            //    Console.WriteLine(+item);
            //}


            /*------------------------------------------------------------------------------------------------------------*/
            //la última cláusula select o group, puede contener una o varias de estas cláusulas opcionales: where, orderby, join, 
            ////int num1, num2;
            //var coleccion =
            //                      from num in array
            //                      group num by num into repetido
            //                      select repetido;

            //foreach (var item in coleccion)
            //{
            //    Console.WriteLine("Número: " + item.Key + " numero repetido " + item.Count());
            //}



            /*------------------------------------------------------------------------------------------------------------*/

            ////CONTAR LA CANTIDAD DE NUEMEROS PARES 
            //IEnumerable<int> coleccion =
            //    from num in array
            //    where (num %2==0 ) 
            //    select num;

            //IEnumerable<int> colleccion = array.Where(num => num % 2 != 0).OrderBy(num => num);  

            //var pares = coleccion.Count();
            //Console.WriteLine("numeros pares hay : "+pares);
            //var impares = colleccion.Count();
            //Console.WriteLine("numeros impares hay : "+impares);
            ////int pares = coleccion.Count();

            ///*------------------------------------------------------------------------------------------------------------*/
            ////LINEA PARA ENCONTRAR DE ENTRE LA COLECCION LOS MENORES A 50 Y PROMEDIARLOS 
            //IEnumerable<int> coleccion=
            //    from mayores in array
            //    where mayores < 50
            //    select mayores;


            //   var promedio = coleccion.Average();
            //Console.WriteLine("el promedio de los mayores a 50: " +promedio);


            //Console.ReadKey();



            /*------------------------------------------------------------------------------------------------------------*/
            //ENCUENTRA UNA LISTA CON LOS NUMEROS PRIMOS 

            //IEnumerable<int> HEREopcion =
            //    from primo in array 
            //    where(primo % primo ==0)&&(primo % Uno)(Uno= ++Uno)
            //    select (primo);

            /*------------------------------------------------------------------------------------------------------------*/

            ////para que genera otra lista con el cuadrado de los numeros 


            //IEnumerable<int> opcion =
            //    from cuadrado in array
            //    select (cuadrado);

            //            var elcuadrado = opcion;

            ////elcuadrado = (opcion*opcion);

            //foreach (int cuadrado in opcion)

            //    Console.WriteLine("aqui el cuadrado anterior"+cuadrado * cuadrado) ; 

            //Console.WriteLine("el cuadrado de la lista es: " + elcuadrado);


            /*-------------------------------------------------------------------------------------------------------------*/


            ////codigo para hacer la supatoria de los numeros

            //for (int i = 0; i < 20; i++)

            //{
            //    array[i] = rnd.Next(0, 20);
            //    Console.WriteLine(array[i] + " ");
            //}

            //Console.WriteLine("Suma total: ");
            ////Mostrar en consola la suma de todos los elementos
            //var suma = array.Sum();
            //Console.WriteLine(suma);



            /*-------------------------------------------------------------------------------------------------------------*/
            ////NUMEROS PRIMOS 
            //var resultado = from NUMERPprimo in array

            //                where NUMERPprimo % NUMERPprimo == 0
            //                //( NUMERPprimo % NUMERPprimo == 0 )(Uno++)

            //                select NUMERPprimo;

            //foreach (var item in resultado)
            //{
            //    Console.WriteLine(item);
            //}

            /*-------------------------------------------------------------------------------------------------------------*/


            ////INTENTO numeros primos 
            //Random rand = new Random();

            ////generar numeros:
            //var aleatorio = Enumerable.Repeat(0, 50).Select(indice => rand.Next(0, 50));
            ////contar los primos 

            //Boolean primo;
            //for (int x = 2; x < aleatorio; x + 1)
            //{
            //}

            /////////////////OTRO INTENTO PIMOS////////////

            /*-------------------------------------------------------------------------------------------------------------*/
            ////ESTA PARTE DEL CODIGO FUNCIONA PARA SABER CUANTAS VECES SE REPITE EL PRIMER VALOR 
            //    if (array[i] == 1)
            //        Uno++;
            //}
            //Console.WriteLine("\nEl valor 1 se repite  " +Uno+ "veces");

        }
    }
}
