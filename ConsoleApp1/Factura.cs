﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    //autor:César F.
    class Factura
    {
        //Clase Factura (Observación:string, idcliente:integer, fecha:DateTime, total:decimal)
        
        public string observacion { get; set; }
        public int venta { get; set; }
        public int idcliente{ get; set; }
        public DateTime fecha { get; set; }
        public decimal total { get; set; }

        //:D
    }
}
